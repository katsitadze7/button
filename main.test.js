describe('Button Test', (x,y,z) => {

  beforeEach(() => {
    cy.visit('/')})

  it('basic elements exist and function correctly', () => {
    cy.get('.button-box').should('exist').and('have.class', 'relative')
    cy.get('.tooltip-box').should('not.be.visible')
    cy.get('.tip').contains('Copy Session ID')
  })

  it('tooltip changes upon click and mouseleave', () => {
    cy.get('.button').click()
    cy.get('.tip').contains('Session ID Copied!')
    cy.get('.button').trigger('mouseleave')
    cy.get('.tip').contains('Copy Session ID')
  })

  it('tooltip should be visible during hover', () => {
    cy.get('.button-box').trigger('mouseover')
    cy.get('.tooltip-box').should('exist')
  })

})
